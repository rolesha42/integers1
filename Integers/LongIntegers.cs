﻿namespace Literals
{
    public static class LongIntegers
    {
        public static long ReturnLongInteger21()
        {
            // TODO #2-1. Return "4,956,185,095,298,947,214" decimal literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger22()
        {
            // TODO #2-2. Return "-1,280,010,762,458,239,942" decimal literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger23()
        {
            // TODO #2-3. Return "-945,783,496,234,828,465" decimal literal.
            throw new NotImplementedException();
        }

        public static ulong ReturnLongInteger24()
        {
            // TODO #2-4. Return "16,269,823,234,523,742,845" decimal literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger25()
        {
            // TODO #2-5. Return "9,223,372,036,854,775,807" hexadecimal literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger26()
        {
            // TODO #2-6. Return "773,738,404,492,802,748" hexadecimal literal.
            throw new NotImplementedException();
        }

        public static ulong ReturnLongInteger27()
        {
            // TODO #2-7. Return "17,977,307,477,258,691,517" hexadecimal literal.
            throw new NotImplementedException();
        }

        public static ulong ReturnLongInteger28()
        {
            // TODO #2-8. Return "14,193,065,825,095,688,383" hexadecimal literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger29()
        {
            // TODO #2-9. Return "4,100,761,908,933,204,629" binary literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger210()
        {
            // TODO #2-10. Return "1,645,102,583,813,967,509" binary literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger211()
        {
            // TODO #2-11. Return "6,148,914,691,236,517,205" binary literal.
            throw new NotImplementedException();
        }

        public static long ReturnLongInteger212()
        {
            // TODO #2-12. Return "8,446,744,073,709,551,615" binary literal.
            throw new NotImplementedException();
        }
    }
}
