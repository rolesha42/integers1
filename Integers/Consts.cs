namespace Literals
{
    public static class Consts
    {
        public static int ReturnLargestInteger()
        {
            // TODO #3-1. Return the largest possible value of the int type.
            throw new NotImplementedException();
        }

        public static int ReturnSmallestInteger()
        {
            // TODO #3-2. Return the smallest possible value of the int type.
            throw new NotImplementedException();
        }

        public static uint ReturnLargestUnsignedInteger()
        {
            // TODO #3-3. Return the largest possible value of the uint type.
            throw new NotImplementedException();
        }

        public static uint ReturnSmallestUnsignedInteger()
        {
            // TODO #3-4. Return the smallest possible value of the uint type.
            throw new NotImplementedException();
        }

        public static long ReturnLargestLongInteger()
        {
            // TODO #3-5. Return the largest possible value of the long type.
            throw new NotImplementedException();
        }

        public static long ReturnSmallestLongInteger()
        {
            // TODO #3-6. Return the smallest possible value of the long type.
            throw new NotImplementedException();
        }

        public static ulong ReturnLargestUnsignedLongInteger()
        {
            // TODO #3-7. Return the largest possible value of the ulong type.
            throw new NotImplementedException();
        }

        public static ulong ReturnSmallestUnsignedLongInteger()
        {
            // TODO #3-8. Return the smallest possible value of the ulong type.
            throw new NotImplementedException();
        }

        public static short ReturnSmallestShortInteger()
        {
            // TODO #3-9. Return the smallest possible value of the short type.
            throw new NotImplementedException();
        }

        public static short ReturnLargestShortInteger()
        {
            // TODO #3-10. Return the largest possible value of the short type.
            throw new NotImplementedException();
        }
    }
}
